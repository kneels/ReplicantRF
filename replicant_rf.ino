/*
	Universal remote control duplicator
	// TODO: Add intro
	MX-FS-03V
	MX-05V

	TODO: add schmitt trigger gate


	IMPORTANT:
	This version only works on the ATmega2560. The ATmega328's EEPROM is not large enough to hold 
	a timings array of considerable length.	
*/
#include <avr/io.h>
#include <avr/eeprom.h>

#define RX_DATA PC7 // Digital pin 30
#define TX_DATA PC6 // Digital pin 31
#define REC_BUTTON PC5 // Digital pin 32
#define PLAY_BUTTON PC4 // Digital pin 33
#define SAVE_BUTTON PC3 // Digital pin 34
#define REC_LED PC2 // Digital pin 35
#define PLAY_LED PC1 // Digital pin 36
#define PORT PORTC
#define DDR DDRC
#define PIN PINC

const uint16_t bufSize = 1024; // Number of edges to record/play. Has to be <= EEPROM length
uint16_t timings[bufSize]; // Array that holds the length of time RX/TX is held low or high
const uint16_t saveButtonDelay = 2000; // The save button needs to be pressed for this many ms before it actually saves
unsigned long saveButtonTime = 0;

void setup()
{	
	DDR |= (_BV(TX_DATA)|_BV(REC_LED)|_BV(PLAY_LED)); // Outputs
	DDR &= ~(_BV(RX_DATA)|_BV(REC_BUTTON)|_BV(PLAY_BUTTON)|_BV(SAVE_BUTTON)); // Inputs

	load(); // Load the timings saved in EEPROM
}

void loop() 
{
    if (bit_is_set(PIN, REC_BUTTON)) // If RECORD button is pressed
    {   
    	PORT |= _BV(REC_LED); // Turn on RECORD LED
    	record(); // Start recording
    }
    else if (bit_is_set(PIN, PLAY_BUTTON)) // If PLAY button is pressed
    {
    	PORT |= _BV(PLAY_LED);	// Turn on PLAY LED
    	play();
    }
    else if (bit_is_set(PIN, SAVE_BUTTON)) // If SAVE button is pressed
    {
    	if (millis() - saveButtonTime > saveButtonDelay)
    	{
    		save();
    	}
    }
    else
    {
		PORT &= ~(_BV(REC_LED)|_BV(PLAY_LED)); // Turn off both LEDS		
   		saveButtonTime = millis();
    }
}

void record()
{
	unsigned long start = 0;
	loop_until_bit_is_clear(PIN, RX_DATA); // Wait for RX_DATA to go LOW

	for (uint16_t i = 0; i < bufSize-1; i+=2)
	{		
		start = micros();
		
		loop_until_bit_is_set(PIN, RX_DATA); // Wait for RX_DATA to go HIGH
		
		timings[i] = micros() - start; // Record how long it stayed LOW
		start = micros();

		loop_until_bit_is_clear(PIN, RX_DATA); // Wait for RX_DATA to go LOW
		
		timings[i+1] = micros() - start; // Record how long it stayed HIGH
	}
}

void play()
{	
	PORT &= ~(_BV(TX_DATA)); // Set TX low

	for (uint16_t i = 0; i < bufSize; ++i)
	{
		// TODO: replace delayMicroseconds function 
		delayMicroseconds(timings[i]); // Keep HIGH or LOW for timings[i] amount of microseconds
		PORT ^= _BV(TX_DATA); // Toggle TX
	}

	PORT &= ~(_BV(TX_DATA)); // Set TX low
}

void save()
{
	for (int i = 0; i < 5; ++i)
	{
		PORT &= ~(_BV(REC_LED)|_BV(PLAY_LED)); // Turn off both LEDS
		delay(50);
		PORT |= (_BV(REC_LED)|_BV(PLAY_LED)); // Turn on both LEDs
		delay(50);
	}

	eeprom_update_block(timings, 0x00, (bufSize * sizeof(uint16_t))); // Write timings array to EEPROM

	for (int i = 0; i < 5; ++i)
	{
		PORT |= (_BV(REC_LED)|_BV(PLAY_LED)); // Turn on both LEDs
		delay(50);
		PORT &= ~(_BV(REC_LED)|_BV(PLAY_LED)); // Turn off both LEDS
		delay(50);
	}
}

void load()
{
	PORT |= (_BV(REC_LED)|_BV(PLAY_LED)); // Turn on both LEDs
	eeprom_read_block(timings, 0x0, (bufSize * sizeof(uint16_t))); // Write EEPROM to timings array
	PORT &= ~(_BV(REC_LED)|_BV(PLAY_LED)); // Turn off both LEDS
}